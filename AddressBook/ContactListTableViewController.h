//
//  ContactListTableViewController.h
//  AddressBook
//
//  Created by Pudel_dev on 19/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactListTableViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSMutableArray * contactListData;

@property (nonatomic,strong) NSArray * allContacts;

@end
