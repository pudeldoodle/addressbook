//
//  ContactListTableViewController.m
//  AddressBook
//
//  Created by Pudel_dev on 19/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import "ContactListTableViewController.h"
#import <AddressBook/AddressBook.h>
#import "Contact.h"

@interface ContactListTableViewController ()

@end

@implementation ContactListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)loadData
{
    self.title = @"Contacts";
    _contactListData = [[NSMutableArray alloc]init];
    [self choisirContact];
}
-(void)choisirContact
{
    CFErrorRef error = NULL;
    ABAddressBookRef contacts = ABAddressBookCreateWithOptions(NULL, &error);
    
    ABAddressBookRequestAccessWithCompletion(contacts, ^(bool granted, CFErrorRef error)
    {
        
    });
    
    if(contacts != nil)
    {
        NSLog(@"ok contacts");
        _allContacts = (__bridge_transfer NSArray*)ABAddressBookCopyArrayOfAllPeople(contacts);
        
        NSUInteger i = 0;
        for (i = 0; i < [_allContacts count]; i++)
        {
            Contact * contact = [[Contact alloc]init];
            ABRecordRef contactAB = (__bridge ABRecordRef)_allContacts[i];
            NSString * firstName = (__bridge_transfer NSString*)ABRecordCopyValue(contactAB, kABPersonFirstNameProperty);
            NSString * lastName = (__bridge_transfer NSString*)ABRecordCopyValue(contactAB, kABPersonLastNameProperty);
            NSString * fullName = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
            
            contact.firstName = firstName;
            contact.lastName = lastName;
            if(firstName && lastName)
            {
                contact.fullName = fullName;
            }
            else if(!firstName && !lastName)
            {
                contact.fullName = @"PAS DEFINI";
            }
            else
            {
                contact.fullName = @"INCOMPLET";
            }
           
            ABMultiValueRef emails = ABRecordCopyValue(contactAB, kABPersonEmailProperty);
            
            NSUInteger j = 0;
            for(j = 0; j < ABMultiValueGetCount(emails); j++)
            {
                NSString * email = (__bridge_transfer NSString*)ABMultiValueCopyValueAtIndex(emails, j);
                if(j == 0)
                {
                    contact.homeEmail = email;
                }
                if(j == 1)
                {
                    contact.workEmail = email;
                }
            }
            [self.contactListData addObject:contact];
        }
        CFRelease(contacts);
    }
    else
    {
        NSLog(@"Error reading Address Book !");
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    //NSLog(@"%i",_contactListData.count);
    return self.contactListData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
    
    if([[self.contactListData objectAtIndex:[indexPath row]] firstName])
    {
        Contact * contactCell = [self.contactListData objectAtIndex:[indexPath row]];
        cell.textLabel.text = contactCell.firstName;
    }
    
    /*Contact * contactCell = [self.contactListData objectAtIndex:[indexPath row]];
    
    if(contactCell.firstName)
    {
        cell.textLabel.text = contactCell.firstName;
    }else{
        cell.textLabel.text = @"";
    }*/
    
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
