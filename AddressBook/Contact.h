//
//  Contact.h
//  AddressBook
//
//  Created by Pudel_dev on 19/06/15.
//  Copyright (c) 2015 Pudel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (nonatomic,weak) NSString * firstName;
@property (nonatomic,weak) NSString * lastName;
@property (nonatomic,weak) NSString * fullName;
@property (nonatomic,weak) NSString * homeEmail;
@property (nonatomic,weak) NSString * workEmail;

@end
